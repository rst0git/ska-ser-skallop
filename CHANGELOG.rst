##########
Change Log
##########

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

[Unreleased]
************

v2.4.0
******

* Refactored `output_file_diff`
    - Renamed to `file_differences`
    - Now returning a bool (whether there are differences or not)
* Raising `XrayException` rather than `SystemExit` in methods

v2.3.0
******

* Update readiness and rectification logic on mvp_control package to be in line with new domain logic for TMC subsystem in MVP.
* Update configuration modules for subarray resource management and scan configuration to be in line with ADR 35
    See https://jira.skatelescope.org/browse/SP-1623 and https://jira.skatelescope.org/browse/SP-1643

v2.2.0
******

* Added new functionality to the xtp scripts:
    - all scripts request the user for the password (if not provided by the `-p` flag) on the terminal without echoing it.
    - `xtp-pull` and `xtp-compare` print in stdout whether the thing being downloaded/compared is a test set or a test scenario, e.g.:
        - `Dowloaded XTP-1839 [Type: Test]`
        - `Dowloaded XTP-1834 [Type: Test Set]`
    - `xtp-compare` output formatted as a unix diff.
    - added a `--dry-run` capability to the `xtp-pull` and `xtp-push` scripts, respectively.

v2.1.1
******

* Changed setenv script to handle new skampi name, see https://gitlab.com/ska-telescope/ska-ser-skallop/-/merge_requests/66.

v2.1.0
******

* Added `xtp-xray-upload` script to upload cucumber formatted result files to JIRA via Xray extension
* SAR-249

v2.0.0
******

* First release of ska-ser-skallop.
* Renamed package from old name skallop to ska_ser_skallop.
* Moving artefacts to a new repository https://artefact.skao.int/.

Added
-----

* Empty Python project directory structure
