"""Common utility like objects used by mvp_management."""


class DisablePost:
    """Class holding flag indicating wether post steps are enabled or disabled."""

    def __init__(self) -> None:
        """Init object."""
        self._disable: bool = False

    def disable(self) -> None:
        """Set the disable flag to true, a context manager will not perform its post steps."""
        self._disable = True

    def enable(self) -> None:
        """Set the disable flag to false, a context manager will not perform its post steps."""
        self._disable = False

    def __bool__(self) -> bool:
        """Allow object to be evaluated as a bool flag.

        :returns: True if the post steps are disabled.
        """
        return self._disable
