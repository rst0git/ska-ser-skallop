from functools import reduce
from typing import Dict, Iterable, TypeVar

T = TypeVar("T")
"""
.. Note to devs: sphinx-autodoc can't document typevars properly right now,
   see https://github.com/agronholm/sphinx-autodoc-typehints/issues/39

A generic type for elements of an iterable.
"""


class IndexedDictionary(Dict):
    def __init__(self):
        super().__init__()
        self._index = 0

    def append(self, value) -> int:
        self._index += 1
        self[self._index] = value
        return self._index


def flatten(_iter: Iterable[Iterable[T]]) -> Iterable[T]:
    if _iter:
        return reduce(lambda x, y: [*x, *y], _iter)
    return _iter
