from ska_ser_skallop.connectors import configuration


def i_can_communicate_with(device_name: str) -> bool:
    try:
        proxy = configuration.get_device_proxy(device_name)
        proxy.ping()
        return True
    except Exception:
        return False
