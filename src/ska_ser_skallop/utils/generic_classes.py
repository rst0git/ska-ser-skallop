from abc import abstractmethod


class Symbol:
    @abstractmethod
    def __str__(self) -> str:
        """
        Return a string representation of the symbol.

        :return: a string representation of the symbol.
        """

    @property
    @abstractmethod
    def enabled(self) -> bool:
        """Whether the device is withing currently set scoped or not.

        :return: True if the device is within the scope.
        """
