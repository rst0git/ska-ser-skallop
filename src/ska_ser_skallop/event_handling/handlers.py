"""Module containing a generic set of messageboard event handlers for tango based event handling."""
import datetime
import logging
from abc import abstractmethod
from typing import List, NamedTuple, Union

from ska_ser_skallop.connectors.configuration import get_device_proxy
from ska_ser_skallop.event_handling import base

logger = logging.getLogger(__name__)


class BaseWaiter(base.MessageHandler):
    """Handler waiting until event value is equal to desired value.

    It achieves this during handling by checking wether the events for a specific
    device and attribute have become equal to the desired value
    upon which it removes the subscription and thus clears waiting for this on the messageboard.
    """

    def __init__(
        self,
        board: base.MessageBoardBase,
        attr: str,
        device: str,
        master: bool,
        handler_annotation="",
    ) -> None:
        """Initialise the object.

        :param board: The messageboard containing the running subscriptions.
        :param attr: The attribute being subscribed to
        :param device: The tango device which will be subscribed to
        :param master: wether events from this tango device
            should result in all subscriptions to be removed from the messageboard
            when the desired value has been received, defaults to False
        :param handler_annotation: particular messages about the handler to annotate logging with
        """
        if master:
            handler_annotation = f"{handler_annotation} (master)"
        self.master = master
        self.attr = attr
        self.device = device
        super().__init__(board, handler_annotation=handler_annotation)
        self.events = []

    def _get_suppress_error_message(self) -> str:
        message = (
            f"Timed out waiting for {self.attr} on {self.device}, but when directly "
            f"read via a read-attribute command the condition was met."
        )
        return message

    def describe_self(self) -> str:
        """Generate a text based description of the handler for debug purposes.

        :return: a text based description of the handler
        """
        return (
            f"handler that waits for events on on device {self.device} "
            f"{self.handler_annotation}"
        )

    def _is_first_element(self) -> bool:
        return len(self.events) == 1

    @abstractmethod
    def condition_met(self) -> bool:
        """Whether the event received indicates the condition to halt waiting has been met.

        Note the method will always ignore the first event as it is not considered a behavioral
        change.

        :return: True if the event received indicates the condition to halt waiting has been met.
        """

    def update(self, event: base.EventDataInt) -> None:
        """Update the state of the object as a consequence of a new event.

        :param event: the new event containing the information to use for updating.
        """
        if event.attr_value:
            self.current_value = self._get_attr_value_as_str(event.attr_value)
            self.events.append(event)
            self.tracer.message(
                f"new event added to list, current list size is {len(self.events)}"
            )

    def handle_event(
        self, event: base.EventDataInt, subscription: base.SubscriptionBase
    ) -> None:
        """Handle a new event from a subscription on a messageboard.

        :param event: The new event that occurred.
        :param subscription: the subscription that generated the event.

        """
        with self.handle_context(event, subscription):
            self.update(event)
            if self.condition_met():
                self.tracer.message("condition met, removing subscription")
                self.unsubscribe(subscription)
                self._annotate_with("condition met")
                if self.master:
                    # unsubscribe any other subscriptions slaved onto this one
                    self.unsubscribe_all()

    def suppress_timeout(self) -> bool:
        """Whether timeout should be surpressed.

        If no events was received on the messageboard within a maximum time it will
        inspect the remaining subscription handlers and determine if the condition should
        warrant a timeout error.

        :returns: True if a timeout should be surpressed

        """
        # check if call can be made by reading required vale
        device = get_device_proxy(self.device)
        attr_value = device.read_attribute(self.attr)
        event = base.EventDataInt(self.device, self.attr, attr_value.value)
        self.update(event)
        if self.condition_met():
            self.tracer.message(
                "condition met after reading value from device when timed out"
            )
            logging.warning(self._get_suppress_error_message())
            return True
        return False


class UnpackedEvent(NamedTuple):
    """Grouped event data as a named tuple."""

    device_name: str
    attr_name: str
    attr_value: str
    time: datetime.datetime


class WaitUntilEqual(BaseWaiter):
    """Handler waiting until event value is equal to desired value.

    It achieves this during handling by checking wether the events for a specific
    device and attribute have become equal to the desired value
    upon which it removes the subscription and thus clears waiting for this on the messageboard.
    """

    def __init__(
        self,
        board: base.MessageBoardBase,
        attr: str,
        value: Union[List[str], str],
        device: str,
        master: bool = False,
        ignore_first: bool = True,
    ) -> None:
        """Initialise the object.

        :param board: The messageboard containing the running subscriptions.
        :param attr: The attribute being subscribed to
        :param value: The desired value the attribute must be in
        :param device: The tango device which will be subscribed to
        :param master: wether events from this tango device
            should result in all subscriptions to be removed from the messageboard
            when the desired value has been received, defaults to False
        :param ignore_first: wether to ignore the first event
            this could be useful if the new event may be the same as the original
            , defaults to True
        """
        self._set_desired(value)
        self._set_properties(board, attr, device, master, ignore_first)

    def _set_desired(self, desired: Union[str, List[str]]) -> None:
        self.desired_value = desired

    def _set_properties(
        self,
        board: base.MessageBoardBase,
        attr: str,
        device: str,
        master: bool,
        ignore_first: bool,
    ) -> None:
        self.ignore_first = ignore_first
        handler_annotation = ""
        super().__init__(
            board, attr, device, master, handler_annotation=handler_annotation
        )

    def _get_suppress_error_message(self) -> str:
        message = (
            f"Timed out waiting for {self.attr} on {self.device} to become {self.desired_value}"
            f" but when directly read the value was equal to {self.desired_value}"
        )
        return message

    def describe_self(self) -> str:
        """Generate a text based description of the handler for debug purposes.

        :return: a text based description of the handler
        """
        return (
            f"handler that waits for {self.attr} on device {self.device} to be equal "
            f"to {self.desired_value}{self.handler_annotation}"
        )

    def _equal(self, current_value: str) -> bool:
        if isinstance(self.desired_value, list):
            return current_value in self.desired_value
        return current_value == self.desired_value

    def condition_met(self) -> bool:
        """Whether the event received indicates the condition to halt waiting has been met.

        Note the method will always ignore the first event as it is not considered a behavioral
        change.

        :return: True if the event received indicates the condition to halt waiting has been met.
        """
        # always ignore the first event as it gets set on subscription an not on
        # behaviour change
        event = self.events[-1]
        if event.err:
            # continue waiting if an error event was received
            return False

        current_value = self._get_attr_value_as_str(event.attr_value)
        if self._equal(current_value):
            if self._is_first_element():
                if self.ignore_first:
                    self.tracer.message(
                        f"ignoring first event event though it is equal to "
                        f"{self.desired_value}"
                    )
                    return False
                return True
            return True
        return False

    def unpack_current_event(self):
        """Get the current event info as a tuple of values after handle event.

        :return: The event info as an named tuple.
        """
        return UnpackedEvent(*self._unpack_event(self.current_event))


class WaitforChange(BaseWaiter):
    """Handler waiting until a change event shows the value is different from original."""

    def __init__(
        self,
        board: base.MessageBoardBase,
        attr: str,
        device: str,
        master: bool = False,
    ) -> None:
        """Init object.

        :param board: The messageboard containing the running subscriptions.
        :param attr: The attribute being subscribed to
        :param device: The tango device which will be subscribed to
        :param master: wether events from this tango device
            should result in all subscriptions to be removed from the messageboard
            when the desired value has been received, defaults to False
        """
        super().__init__(board, attr, device, master, handler_annotation="")

    @abstractmethod
    def condition_met(self) -> bool:
        """Whether the event received indicates the condition to halt waiting has been met.

        Note the method will always ignore the first event as it is not considered a behavioral
        change.

        :return: True if the event received indicates the condition to halt waiting has been met.
        """
        event = self.events[-1]
        if self._is_first_element():
            self._initial_value = self._get_attr_value_as_str(event.attr_value)
            return False
        if event.err:
            # continue waiting if an error event was received
            self.tracer.message("new event ignored as it is in error")
            return False
        current_value = self._get_attr_value_as_str(event.attr_value)
        if current_value == self._initial_value:
            self.tracer.message(
                f"new event ignored as it is the same as the original value ({current_value})"
            )
            return False
        return True


class WaitForOrderedChange(WaitUntilEqual):
    """Handler waiting until correct order of events occurred.

    This works similar to :py:class:'WaitUntilEqual` except the desired value is now a list or
    ordered states that must be followed
    """

    def __init__(
        self,
        board: base.MessageBoardBase,
        attr: str,
        value: List[str],
        device: str,
        master: bool = False,
        ignore_first: bool = True,
        wire_tap: Union[None, base.WireTap] = None,
    ) -> None:
        """Initialise the object.

        :param board: The messageboard containing the running subscriptions.
        :param attr: The attribute being subscribed to
        :param value: The desired ordered values the attribute must be in
        :param device: The tango device which will be subscribed to
        :param master: wether events from this tango device
            should result in all subscriptions to be removed from the messageboard
            when the desired value has been received, defaults to False
        :param ignore_first: wether to ignore the first event
            this could be useful if the new event may be the same as the original
            , defaults to True
        :param wire_tap: An additional object that, if given
            will be used tap in on the occurrences of events, defaults to None

        """
        self._set_desired(value)
        self._set_properties(board, attr, device, master, ignore_first)
        self._wire_tap = wire_tap
        self._current_value = ""

    def _set_desired(self, desired: List[str]) -> None:
        self.desired_values = desired.copy()
        self.desired_value = self.desired_values[-1]

    def _expected_event(self) -> str:
        if not self._is_complete():
            return self.desired_values[0]
        return "None (all events received)"

    def _is_partially_correct(self) -> bool:
        current_event = self._current_event()
        if not current_event.err:
            if current_event.attr_value:
                current_value = self._get_attr_value_as_str(current_event.attr_value)
                expected_value = self._expected_event()
                if current_value == expected_value:
                    self.desired_values.pop(0)
                    return True
                if current_value in self.desired_values:
                    self.tracer.message(
                        f"WARNING: current event has a value:{current_value} ahead of "
                        f"what is expected: {expected_value}, will forward expectancies"
                    )
                    while self.desired_values[0] != current_value:
                        self.desired_values.pop(0)
                    self.desired_values.pop(0)
                    return True
                self.tracer.message(
                    f"WARNING: current event has a value:{current_value} different "
                    f"than what is expected: {expected_value}"
                )
        return False

    def _is_complete(self) -> bool:
        return len(self.desired_values) == 0

    def _current_event(self) -> base.EventDataInt:
        return self.events[-1]

    def update(self, event: base.EventDataInt) -> None:
        """Update the state of the object as a consequence of a new event.

        :param event: the new event containing the information to use for updating.
        """
        super().update(event)
        if self._wire_tap:
            if self.ignore_first:
                if not self._is_first_element():
                    self._wire_tap.tap_in_on_event(event)
            else:
                self._wire_tap.tap_in_on_event(event)

    def condition_met(self) -> bool:
        """Whether the event received indicates the condition to halt waiting has been met.

        Note the method will always ignore the first event as it is not considered a behavioral
        change.

        :return: True if the event received indicates the condition to halt waiting has been met.
        """
        # always ignore the first returned event unless ignore_first
        if self._is_first_element():
            if self.ignore_first:
                return False
        if self._is_partially_correct():
            # e.g. the current event is what it is expected to be
            self.tracer.message(
                f"current event is correct, waiting for {self._expected_event()}"
            )
            return self._is_complete()  # e.g. the entire sequence is equivalent
        return False


class ObserveLogEvent(base.MessageHandler):
    """Handle messages originating from a log consumer.

    The handler will allow for creating human readable message about the log message.
    """

    def __init__(self, board, attr: str, consumer: str) -> None:
        """Initialise the object.

        :param board: The messageboard containing the running subscriptions.
        :param attr: The attribute being subscribed to
        :param consumer: [description]

        """
        self.attr = attr
        self.consumer = consumer
        super().__init__(board)
        self.expendable = True

    def describe_self(self) -> str:
        """Generate a text based description of the handler for debug purposes.

        :return: a text based description of the handler
        """
        return (
            f"handler that records log messages relayed to {self.consumer} and stops "
            "after a predefined timeout set on the messageboard"
        )

    def suppress_timeout(self) -> bool:
        """Whether timeout should be surpressed.

        If no events was received on the messageboard within a maximum time it will
        inspect the remaining subscription handlers and determine if the condition should
        warrant a timeout error.

        :returns: True if a timeout should be surpressed

        """
        return True

    def print_event(self, event: base.EventDataInt, ignore_first=None) -> str:
        """Generate a string based description of the event that has occurred.

        :param event: [description]
        :param ignore_first: [description], defaults to None
        :return: [description]
        """
        _, _, attr_value, time = self._unpack_event(event)
        message = f"Log Message: \n{attr_value:<10}"
        self.board.log(message, time, label="log")
        return message


class ObserveEvent(base.MessageHandler):
    """Handles events by simply observing them for diagnostic purposes."""

    def __init__(self, board, attr: str, device: str) -> None:
        """Initialise the object.

        :param board: The messageboard containing the running subscriptions.
        :param attr: The attribute being subscribed to
        :param device: The tango device which will be subscribed to
        """
        self.attr = attr
        self.device = device
        super().__init__(board)
        self.expendable = True

    def describe_self(self) -> str:
        """Generate a text based description of the handler for debug purposes.

        :return: a text based description of the handler
        """
        return (
            f"handler that records {self.attr} on device {self.device} and stops after "
            "a predefined timeout set on the messageboard"
        )

    def suppress_timeout(self) -> bool:
        """Whether timeout should be surpressed.

        If no events was received on the messageboard within a maximum time it will
        inspect the remaining subscription handlers and determine if the condition should
        warrant a timeout error.

        :returns: True if a timeout should be surpressed

        """
        return True

    def print_event(self, event, ignore_first=None) -> str:
        """Print the event in human readable format.

        Note it will be set to print out always the first event
        as this aids in observation.

        :param event: The event from which the output will be generated
        :param ignore_first: whether to ignore the first event, this will be
            ignored for this implementation.
        :returns: string representing the event in  human readable format.
        """
        return super().print_event(event, False)


class OccurrenceRecord(NamedTuple):
    """Grouping of a variables related to an event occurrence."""

    device_name: str
    attr_name: str
    attr_value: str
    time: datetime.datetime


class Recorder:
    """Object that can be used to record events occurring during a waiting period.

    Afterwards the object can be used to check if any events came after a specific event.
    """

    def __init__(self) -> None:
        """Initialise the object."""
        self._occurrences: List[OccurrenceRecord] = []

    def record(self, handler: base.MessageHandlerBase):
        """Record an event occurrence after being handled by the handler.

        Note an event will only be recorded for WaitUntilEqual types of handlers.

        :param handler: the handler supplied for the event.
        """
        if isinstance(handler, WaitUntilEqual):
            occurrence = OccurrenceRecord(*handler.unpack_current_event())
            self._occurrences.append(occurrence)
            self._occurrences.sort(key=lambda item: item.time.timestamp(), reverse=True)

    def get_devices_transitioned_after(
        self, device_name: str, value: Union[str, None] = None
    ):
        """Get a list of events that occurred after a given device.

        If the device value is given it wil start to look for events
        for which the attribute is equal to that value, otherwise it will
        look after the last event for that device occurred.

        :param device_name: the subject device.ame: str
        :param value: a value to mark a specific event, defaults to None
            in which case it will start look only from the latest occurrence.
        :return: a list of occurrences (including the subject device)
        """
        items: List[OccurrenceRecord] = []
        for item in self._occurrences:
            if item.device_name == device_name:
                if value:
                    if item.attr_value != value:
                        continue
                    else:
                        items.append(item)
                        break
                items.append(item)
                break
            items.append(item)
        return items

    def assert_no_devices_transitioned_after(
        self, device_name: str, value: Union[str, None] = None
    ):
        """Check if within the reccorded events if any occurred after the given device.

        If a value is given the check will take into consideration the event with that specific
        value.

        :param device_name: the subject device under investigation
        :param value: the event value the device attribute should be in, defaults to None
            in which case it will look at the last event occurrence for that device.
        :raises AssertionError: if any events occurred after the subject device.
        """
        items = self.get_devices_transitioned_after(device_name, value)
        if items[:-1]:
            items.sort(key=lambda item: item.time.timestamp())
            print_out = "".join(
                [
                    f"{item.time.isoformat():<40}"
                    f"{item.device_name:<40}"
                    f"{item.attr_name:<40}"
                    f"{item.attr_value}\n"
                    for item in items
                ]
            )
            raise AssertionError(
                "the following devices transition to the desired"
                f" state after {device_name}:\n{print_out}"
            )
