"""
Subscribing allows the monitoring of events generated from diverse producers at a
central place. If you have producer A and B, for which you expect over the course of a
test to produce events 1,2,3; then you set up a message board to which these producers
can post if and when they generate the expected event. Then at the appropriate time, you
can access these events, check their order, analyse their timing and or verify their
content within your test.

The example below illustrates the concept:


.. literalinclude:: ../../../src/ska_ser_skallop/subscribing/examples/subscribing_example.py

"""
