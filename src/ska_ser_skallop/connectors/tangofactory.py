"""Factory for constructing tango implementations of connector objects."""
from typing import cast

from ska_ser_skallop.connectors.tango.tangoqueries import TangoDeviceQuery
from ska_ser_skallop.mvp_control.base import AbstractDeviceProxy, AbstractDevicesQuery
from ska_ser_skallop.subscribing.base import Producer
from ska_ser_skallop.utils import env

from .base import AbstractFactory

if env.build_in_testing():
    from tango import DeviceProxy  # type: ignore


class DevicePool:
    """
    Class that delivers tango devices proxies.

    It uses the same proxy instance for a device with the same name.
    """

    def __init__(self) -> None:
        """Initialise object."""
        self.devices = {}

    def get_device(self, device_name: str) -> "DeviceProxy":
        """Return a tango device from a pool if existing tango instances.

        If the device proxy instance already exists a new one will not be generated.

        :param device_name: The device name (FQDN)
        :type device_name: str
        :return: The tango device proxy instance
        """
        if device_name in self.devices.keys():
            return self.devices[device_name]
        device = DeviceProxy(device_name)
        self.devices[device_name] = device
        return device


def get_tango_factory():
    """Generate a tango based factory.

    This factory will construct connectors using tango.

    :return: the tango based factory
    """
    assert env.build_in_testing()
    return Tangofactory()


class Tangofactory(AbstractFactory):
    """Factory that construct connectors using tango."""

    def __init__(self) -> None:
        """Initialise object."""
        super().__init__()
        self.pool = DevicePool()

    def get_device_proxy(self, name: str, fast_load=False) -> AbstractDeviceProxy:
        """Construct and configure a tango device proxy implementation.

        :param name: The FQDN name for the tango device
        :type name: str
        :param fast_load:  whether device (in case of remote implementation) should be loaded
            fast, defaults to False
        :type fast_load: bool, optional
        :return: the constructed object
        """
        # note, Tango Device Proxy is considered a "ducttype" sub class of AbstractDeviceProxy
        # the main reason for this is that we can not directly subclass TangoDevice Proxy
        deviceproxy = cast(AbstractDeviceProxy, self.pool.get_device(name))
        return deviceproxy

    def get_devices_query(self) -> AbstractDevicesQuery:
        """Construct and configure a devices query implementation.

        :return: the constructed object
        """
        return TangoDeviceQuery(self)

    def get_producer(self, name: str, fast_load=False) -> Producer:
        """Construct and configure a producer implementation.

        Note this will be the same as for get devices as tango device is
        considered a sub class of producer (ducked typed).

        :param name: [description]
        :type name: str
        :param fast_load: [description], defaults to False
        :type fast_load: bool, optional
        :return: [description]
        """
        # note, Tango Device Proxy is considered a "ducttype" sub class of Producer
        # the main reason for this is that we can not directly subclass TangoDevice Proxy
        # since Tango Device Proxy is considered a "ducttyped" subclass of Abstract Device Proxy
        # it must also be one for Producer because Abstract Device Proxy is a subtype of Producer
        # "All men are mortal, Socrates is a man, ergo..."
        producer = cast(Producer, self.get_device_proxy(name))
        return producer
