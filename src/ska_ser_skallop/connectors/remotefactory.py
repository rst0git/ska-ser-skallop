"""Constructs objects for implementing connectors via a remote tango bridge option."""
from typing import Dict

from ska_ser_skallop.connectors.remoting.remote_devices import (
    RemoteDeviceProxy,
    RemoteDevicesQuery,
)
from ska_ser_skallop.connectors.remoting.tangobridge.factories import TBridgeFactory
from ska_ser_skallop.connectors.remoting.tangobridge.tangobridge import TangoBridge
from ska_ser_skallop.mvp_control.base import AbstractDeviceProxy, AbstractDevicesQuery
from ska_ser_skallop.subscribing.base import Producer
from ska_ser_skallop.utils import env

from .base import AbstractFactory


def get_remote_factory(bridge_factory: TBridgeFactory = None) -> AbstractFactory:
    """Return a factory for implementing connectors via a remote tango bridge option.

    :param bridge_factory: the tango bridge implementation to use, defaults to None
    :type bridge_factory: TBridgeFactory
    :return: the remote factory
    """
    assert not env.build_in_testing()
    if not bridge_factory:
        bridge_factory = TBridgeFactory()
    return RemoteFactory(bridge_factory)


class RemoteFactory(AbstractFactory):
    """Factory for implementing connectors via a remote tango bridge option."""

    def __init__(self, bridge_factory: TBridgeFactory) -> None:
        """[Initialise object.

        :param bridge_factory: the tango bridge implementation to use
        :type bridge_factory: TBridgeFactory
        """
        super().__init__()
        self._bridge_factory = bridge_factory
        self._tango_bridge = TangoBridge(bridge_factory)
        self._device_pool: Dict[str, AbstractDeviceProxy] = {}

    def get_device_proxy(self, name: str, fast_load=True) -> AbstractDeviceProxy:
        """Return a device proxy implementation using remote tango bridge.

        :param name: The FQDN name for the tango device
        :type name: str
        :param fast_load: whether device should be loaded fast, defaults to False
        :type fast_load: bool, optional
        :return: device proxy implementation
        """
        if fast_load:
            return RemoteDeviceProxy(name, self._tango_bridge, fastload=fast_load)
        if self._device_pool.get(name):
            return self._device_pool[name]
        proxy = RemoteDeviceProxy(name, self._tango_bridge, fastload=fast_load)
        self._device_pool[name] = proxy
        return proxy

    def get_devices_query(self) -> AbstractDevicesQuery:
        """Generate a devices query implementation using tango bridge.

        :return: the devices query implementation
        """
        return RemoteDevicesQuery(self._tango_bridge)

    def get_producer(self, name: str, fast_load=False) -> Producer:
        """Generate a producer implementation using tango bridge.

        Note this will be the same as for get devices as tango device is
        considered a sub class of producer (ducked typed)

        :param name: The producer name
        :type name: str
        :param fast_load: [description], defaults to False
        :type fast_load: bool, optional
        :return: the producer implementation
        """
        return self.get_device_proxy(name, fast_load)
