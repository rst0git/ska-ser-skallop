"""Allows for connecting to tango devices from a remote client going through a tango gql service.

The remoting package implements the Abstract Tango Device and Devices Query as a client making
remote calls to a tango device via a tango bridge service.
"""
