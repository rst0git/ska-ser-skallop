"""Implements a tango bridge to a tango gql service for rest and websocket connections.

The the tango bridge is essentially a facade in front of a rest and and websocket controller, each
one responsible for monitoring the connection health and ensuring successful calls to the services.
The websocket service indirectly implements tango based subscriptions to devices that gets used by
a subscribing module before being access by the tango bridge.
"""
