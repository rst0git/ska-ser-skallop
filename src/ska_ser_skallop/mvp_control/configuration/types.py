from enum import Enum, auto


class CompositionType(Enum):
    STANDARD = auto()


class ScanConfigurationType(Enum):
    STANDARD = auto()


class Composition:
    def __init__(self, conf_type: CompositionType) -> None:
        self.conf_type = conf_type


class ScanConfiguration:
    def __init__(self, conf_type: ScanConfigurationType) -> None:
        self.conf_type = conf_type


class CompositionByFile(Composition):
    """Inherits from Configuration"""

    def __init__(self, location: str, conf_type: CompositionType) -> None:
        super().__init__(conf_type)
        self.location = location


class ScanConfigurationByFile(ScanConfiguration):
    """Inherits from Configuration"""

    def __init__(self, location: str, conf_type: ScanConfigurationType) -> None:
        super().__init__(conf_type)
        self.location = location
