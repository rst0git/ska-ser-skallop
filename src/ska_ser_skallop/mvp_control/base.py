import functools
from abc import abstractmethod
from functools import reduce
from typing import Any, Dict, List, Type, TypedDict

from ska_ser_skallop.subscribing.base import AttributeInt, Producer


def stringify(the_list: List[str]) -> str:
    if the_list:
        return reduce(lambda x, y: f"{x}\n{y}", the_list)
    return "none"


class SideCarException(Exception):
    """Used to signal an general exception occurred during a particular
    routine, the routine of which is specified by having a more specific exception class
    derived from this one. The handler of the exception can then retrieve the original
    general exception that was tagged along with the additional knowledge of when it
    occurred
    """

    def __init__(self, base_exception: Exception) -> None:
        self.base_exception = base_exception


def except_it(E: Type[SideCarException]):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            try:
                result = func(*args, **kwargs)
            except Exception as e:
                raise E(e) from e
            return result

        return wrapper

    return decorator


class Spec(TypedDict):

    name: str
    attr: str


class AbstractDevicesQuery:
    @abstractmethod
    def query(self, device_list: List[str], attr: str, timeout=3) -> Dict[str, Any]:
        pass

    @abstractmethod
    def complex_query(self, device_specs: List[Spec], timeout=3) -> Dict[str, Any]:
        pass


class AbstractDeviceProxy(Producer):
    @abstractmethod
    def read_attribute(self, attr_name: str, *args, **kwargs) -> AttributeInt:
        pass

    @abstractmethod
    def ping(self) -> float:
        pass

    @abstractmethod
    def command_inout(self, cmd_name, cmd_param=None, *args, **kwargs) -> Any:
        pass

    @abstractmethod
    def State(self) -> str:
        pass

    @abstractmethod
    def write_attribute(self, attr_name: str, value: Any, *args, **kwargs) -> Any:
        """Write a given value to the named attribute."""

    def set_timeout_millis(self, timeout_in_ms: int) -> None:
        pass
