"""Module containing an implementation of Entrypoint as a composite of Observation Steps"""
from typing import List, Union

from ska_ser_skallop.event_handling.builders import (
    MessageBoardBuilder,
    get_message_board_builder,
)
from ska_ser_skallop.mvp_control.configuration.types import (
    Composition,
    ScanConfiguration,
)

from .base import (
    AbortStep,
    AssignResourcesStep,
    ConfigureStep,
    EntryPoint,
    ObservationExecutionStep,
    ObservationStep,
    ObsResetStep,
    ScanStep,
    SubarraySetupStep,
)


class NotImplementedStep(ObservationStep):
    """Observation Step derived class rasing an NotImplementedError when called."""

    def do(self):
        """Implements do as a raised NotImplementedError

        :raises NotImplementedError: when called
        """
        raise NotImplementedError()

    def undo(self):
        """Implements undo as a raised NotImplementedError

        :raises NotImplementedError: when called
        """
        raise NotImplementedError()

    def set_wait_for_do(self) -> Union[MessageBoardBuilder, None]:
        """Implements set_wait_for_do as a raised NotImplementedError

        :raises NotImplementedError: when called
        """
        raise NotImplementedError()

    def set_wait_for_doing(self) -> Union[MessageBoardBuilder, None]:
        """Implements set_wait_for_doing as a raised NotImplementedError

        :raises NotImplementedError: when called
        """
        raise NotImplementedError()

    def set_wait_for_undo(self) -> Union[MessageBoardBuilder, None]:
        """Implements set_wait_for_undo as a raised NotImplementedError

        :raises NotImplementedError: when called
        """
        raise NotImplementedError()


class NotImplementedSubarraySetupStep(SubarraySetupStep):
    """Derived SubarraySetupStep class rasing an NotImplementedError when called."""

    def do(self, sub_array_id: int):
        """Implements do as a raised NotImplementedError

        :raises NotImplementedError: when called
        """
        raise NotImplementedError()

    def undo(self, sub_array_id: int):
        """Implements undo as a raised NotImplementedError

        :raises NotImplementedError: when called
        """
        raise NotImplementedError()

    def set_wait_for_do(self, sub_array_id: int) -> Union[MessageBoardBuilder, None]:
        """Implements set_wait_for_do as a raised NotImplementedError

        :raises NotImplementedError: when called
        """
        raise NotImplementedError()

    def set_wait_for_doing(self, sub_array_id: int) -> MessageBoardBuilder:
        """Implements set_wait_for_doing as a raised NotImplementedError

        :raises NotImplementedError: when called
        """
        raise NotImplementedError()

    def set_wait_for_undo(self, sub_array_id: int) -> MessageBoardBuilder:
        """Implements set_wait_for_undo as a raised NotImplementedError

        :raises NotImplementedError: when called
        """
        raise NotImplementedError()


class NotImplementedObservationExecutionStep(ObservationExecutionStep):
    """Derived ObservationExecutionStep class rasing an NotImplementedError when called."""

    def do(self, sub_array_id: int):
        """Implements do as a raised NotImplementedError

        :raises NotImplementedError: when called
        """
        raise NotImplementedError()

    def undo(self, sub_array_id: int):
        """Implements undo as a raised NotImplementedError

        :raises NotImplementedError: when called
        """
        raise NotImplementedError()

    def set_wait_for_do(
        self, sub_array_id: int, receptors: List[int]
    ) -> Union[MessageBoardBuilder, None]:
        """Implements set_wait_for_do as a raised NotImplementedError

        :raises NotImplementedError: when called
        """
        raise NotImplementedError()

    def set_wait_for_doing(
        self, sub_array_id: int, receptors: List[int]
    ) -> MessageBoardBuilder:
        """Implements set_wait_for_doing as a raised NotImplementedError

        :raises NotImplementedError: when called
        """
        raise NotImplementedError()

    def set_wait_for_undo(
        self, sub_array_id: int, receptors: List[int]
    ) -> MessageBoardBuilder:
        """Implements set_wait_for_undo as a raised NotImplementedError

        :raises NotImplementedError: when called
        """
        raise NotImplementedError()


class NotImplementedAssignResourcesStep(
    AssignResourcesStep, NotImplementedSubarraySetupStep
):
    """Derived AssignResourcesStep class rasing an NotImplementedError when called."""

    def do(
        self,
        sub_array_id: int,
        dish_ids: List[int],
        composition: Composition,
        sb_id: str,
    ):
        """Implements do as a raised NotImplementedError

        :raises NotImplementedError: when called
        """
        raise NotImplementedError()


class NotImplementedConfigureStep(
    ConfigureStep, NotImplementedObservationExecutionStep
):
    """Derived ConfigureStep class rasing an NotImplementedError when called."""

    def do(
        self,
        sub_array_id: int,
        dish_ids: List[int],
        configuration: ScanConfiguration,
        sb_id: str,
        duration: float,
    ):
        """Implements do as a raised NotImplementedError

        :raises NotImplementedError: when called
        """
        raise NotImplementedError()


class NotImplementedScanStep(ScanStep, NotImplementedObservationExecutionStep):
    """Derived ScanStep class rasing an NotImplementedError when called."""

    pass


class NotImplementedObsResetStep(ObsResetStep, NotImplementedObservationExecutionStep):
    """Derived ObsResetStep class rasing an NotImplementedError when called."""

    pass


class NotImplementedAbortStep(AbortStep, NotImplementedSubarraySetupStep):
    """Derived AbortStep class rasing an NotImplementedError when called."""

    pass


class CompositeEntryPoint(EntryPoint):
    """An Base Implimentation of EntryPoint using ObservationStep objects.

    To implement a more specific Entrypoint for an interface point, override the
    particular step object (:py:class:`ObservationStep`)
    with a more specific class e.g.:

    .. code-block:: python

        def __init__(self) -> None:
            super().__init__()
            self.set_online_step = MySetOnlineStep()

    """

    set_online_step: ObservationStep = NotImplementedStep()
    start_up_step: ObservationStep = NotImplementedStep()
    assign_resources_step: AssignResourcesStep = NotImplementedAssignResourcesStep()
    configure_scan_step: ConfigureStep = NotImplementedConfigureStep()
    scan_step: ScanStep = NotImplementedScanStep()
    obsreset_step: ObsResetStep = NotImplementedObsResetStep()
    abort_step: AbortStep = NotImplementedAbortStep()

    def set_waiting_for_offline_components_to_become_online(
        self,
    ) -> Union[MessageBoardBuilder, None]:
        return self.set_online_step.set_wait_for_do()

    def set_offline_components_to_online(self):
        self.set_online_step.do()

    def set_telescope_to_running(self):
        self.start_up_step.do()

    def set_telescope_to_standby(self):
        self.start_up_step.undo()

    def tear_down_subarray(self, sub_array_id: int):
        self.assign_resources_step.undo(sub_array_id)

    def compose_subarray(
        self,
        sub_array_id: int,
        dish_ids: List[int],
        composition: Composition,
        sb_id: str,
    ):
        self.assign_resources_step.do(sub_array_id, dish_ids, composition, sb_id)

    def clear_configuration(self, sub_array_id: int):
        self.configure_scan_step.undo(sub_array_id)

    def configure_subarray(
        self,
        sub_array_id: int,
        dish_ids: List[int],
        configuration: ScanConfiguration,
        sb_id: str,
        duration: float,
    ):
        self.configure_scan_step.do(
            sub_array_id, dish_ids, configuration, sb_id, duration
        )

    def scan(self, sub_array_id: int):
        self.scan_step.do(sub_array_id)

    def set_wating_for_scan_completion(
        self, sub_array_id: int, receptors: List[int]
    ) -> Union[MessageBoardBuilder, None]:
        return self.scan_step.set_wait_for_do(sub_array_id, receptors)

    def set_wating_for_start_up(self) -> Union[MessageBoardBuilder, None]:
        return self.start_up_step.set_wait_for_do()

    def set_waiting_for_assign_resources(
        self, sub_array_id: int
    ) -> Union[MessageBoardBuilder, None]:
        return self.assign_resources_step.set_wait_for_do(sub_array_id)

    def set_waiting_for_configure(
        self, sub_array_id: int, receptors: List[int]
    ) -> Union[MessageBoardBuilder, None]:
        return self.configure_scan_step.set_wait_for_do(sub_array_id, receptors)

    def set_waiting_until_configuring(
        self, sub_array_id: int, receptors: List[int]
    ) -> Union[MessageBoardBuilder, None]:
        return self.configure_scan_step.set_wait_for_doing(sub_array_id, receptors)

    def set_waiting_until_scanning(
        self, sub_array_id: int, receptors: List[int]
    ) -> Union[MessageBoardBuilder, None]:
        return self.scan_step.set_wait_for_doing(sub_array_id, receptors)

    def set_waiting_until_resourcing(
        self, sub_array_id: int
    ) -> Union[MessageBoardBuilder, None]:
        return self.assign_resources_step.set_wait_for_doing(sub_array_id)

    def set_waiting_for_clear_configure(
        self, sub_array_id: int, receptors: List[int]
    ) -> Union[MessageBoardBuilder, None]:
        return self.configure_scan_step.set_wait_for_undo(sub_array_id, receptors)

    def set_waiting_for_obsreset(
        self, sub_array_id: int, receptors: List[int]
    ) -> Union[MessageBoardBuilder, None]:
        return self.obsreset_step.set_wait_for_do(sub_array_id, receptors)

    def set_waiting_for_release_resources(
        self, sub_array_id: int
    ) -> Union[MessageBoardBuilder, None]:
        return self.assign_resources_step.set_wait_for_undo(sub_array_id)

    def set_wating_for_shut_down(
        self,
    ) -> Union[MessageBoardBuilder, None]:
        return self.start_up_step.set_wait_for_undo()

    def abort_subarray(self, sub_array_id: int):
        self.abort_step.do(sub_array_id)

    def reset_subarray(self, sub_array_id: int):
        self.obsreset_step.do(sub_array_id)


class NoOpStep(ObservationStep):
    """Type of step that does not do anything."""

    def do(self):
        pass

    def set_wait_for_do(self) -> Union[MessageBoardBuilder, None]:
        return get_message_board_builder()

    def set_wait_for_doing(self) -> Union[MessageBoardBuilder, None]:
        return get_message_board_builder()

    def set_wait_for_undo(self) -> Union[MessageBoardBuilder, None]:
        return get_message_board_builder()

    def undo(self) -> Union[MessageBoardBuilder, None]:
        pass
