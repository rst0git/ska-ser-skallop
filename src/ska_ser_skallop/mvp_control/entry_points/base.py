import logging
from abc import abstractmethod
from typing import List, Union

from ska_ser_skallop.event_handling.builders import MessageBoardBuilder
from ska_ser_skallop.mvp_control.configuration.types import (
    Composition,
    ScanConfiguration,
)

logger = logging.getLogger(__name__)


class EntryPoint:
    def __init__(self) -> None:
        pass

    @abstractmethod
    def set_waiting_for_offline_components_to_become_online(
        self,
    ) -> Union[MessageBoardBuilder, None]:
        pass

    @abstractmethod
    def set_offline_components_to_online(self):
        pass

    @abstractmethod
    def set_telescope_to_running(self):
        pass

    @abstractmethod
    def set_telescope_to_standby(self):
        pass

    @abstractmethod
    def tear_down_subarray(self, sub_array_id: int):
        pass

    @abstractmethod
    def compose_subarray(
        self,
        sub_array_id: int,
        dish_ids: List[int],
        composition: Composition,
        sb_id: str,
    ):
        pass

    @abstractmethod
    def clear_configuration(self, sub_array_id: int):
        pass

    @abstractmethod
    def configure_subarray(
        self,
        sub_array_id: int,
        dish_ids: List[int],
        configuration: ScanConfiguration,
        sb_id: str,
        duration: float,
    ):
        pass

    @abstractmethod
    def scan(self, sub_array_id: int):
        pass

    @abstractmethod
    def set_wating_for_scan_completion(
        self, sub_array_id: int, receptors: List[int]
    ) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_wating_for_start_up(self) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_waiting_for_assign_resources(
        self, sub_array_id: int
    ) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_waiting_for_configure(
        self, sub_array_id: int, receptors: List[int]
    ) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_waiting_until_configuring(
        self, sub_array_id: int, receptors: List[int]
    ) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_waiting_until_scanning(
        self, sub_array_id: int, receptors: List[int]
    ) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_waiting_until_resourcing(self, sub_array_id: int) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_waiting_for_clear_configure(
        self, sub_array_id: int, receptors: List[int]
    ) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_waiting_for_obsreset(
        self, sub_array_id: int, receptors: List[int]
    ) -> MessageBoardBuilder:
        """[summary]

        :param sub_array_id: [description]
        :param receptors: [description]

        :return: [description]
        """

    @abstractmethod
    def set_waiting_for_release_resources(
        self, sub_array_id: int
    ) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_wating_for_shut_down(
        self,
    ) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def abort_subarray(self, sub_array_id: int):
        pass

    @abstractmethod
    def reset_subarray(self, sub_array_id: int):
        pass


class ObservationStep:
    """Defines the abstract logic for a step in an observation execution flow."""

    @abstractmethod
    def set_wait_for_do() -> Union[MessageBoardBuilder, None]:
        pass

    @abstractmethod
    def set_wait_for_doing() -> Union[MessageBoardBuilder, None]:
        pass

    @abstractmethod
    def do():
        pass

    @abstractmethod
    def set_wait_for_undo() -> Union[MessageBoardBuilder, None]:
        pass

    @abstractmethod
    def undo():
        pass


class SubarraySetupStep(ObservationStep):
    @abstractmethod
    def do(self, sub_array_id: int):
        pass

    @abstractmethod
    def set_wait_for_do(self, sub_array_id: int) -> Union[MessageBoardBuilder, None]:
        pass

    @abstractmethod
    def set_wait_for_doing(self, sub_array_id: int) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def undo(self, sub_array_id: int):
        pass

    @abstractmethod
    def set_wait_for_undo(self, sub_array_id: int) -> MessageBoardBuilder:
        pass


class AssignResourcesStep(SubarraySetupStep):
    @abstractmethod
    def do(
        self,
        sub_array_id: int,
        dish_ids: List[int],
        composition: Composition,
        sb_id: str,
    ):
        pass


class ObservationExecutionStep(ObservationStep):
    @abstractmethod
    def do(self, sub_array_id: int):
        pass

    @abstractmethod
    def undo(self, sub_array_id: int):
        pass

    @abstractmethod
    def set_wait_for_do(
        self, sub_array_id: int, receptors: List[int]
    ) -> Union[MessageBoardBuilder, None]:
        pass

    @abstractmethod
    def set_wait_for_doing(
        self, sub_array_id: int, receptors: List[int]
    ) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_wait_for_undo(
        self, sub_array_id: int, receptors: List[int]
    ) -> Union[MessageBoardBuilder, None]:
        pass


class ConfigureStep(ObservationExecutionStep):
    @abstractmethod
    def do(
        self,
        sub_array_id: int,
        dish_ids: List[int],
        configuration: ScanConfiguration,
        sb_id: str,
        duration: float,
    ):
        pass


class ScanStep(ObservationExecutionStep):
    @abstractmethod
    def do(self, sub_array_id: int):
        pass


class ObsResetStep(ObservationExecutionStep):
    pass


class AbortStep(SubarraySetupStep):
    pass
