from .base import DeviceAllocation
from .tango_infra import ClusterWithDevices, DevicesChart, TangoBasedRelease

__all__ = ("DevicesChart", "DeviceAllocation")


def get_mvp_cluster() -> ClusterWithDevices:
    return ClusterWithDevices()


def get_mvp_release() -> TangoBasedRelease:
    cluster = ClusterWithDevices()
    release = cluster.release
    assert release, "Unable to get a release on the current cluster"
    return release
