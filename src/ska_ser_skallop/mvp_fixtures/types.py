"""Module acting as facade to internal skallop objects needed for type referencing."""

__all__ = ["DeviceLogSpec", "PlaySpec"]


from ska_ser_skallop.event_handling.logging import DeviceLogSpec
from ska_ser_skallop.mvp_control.event_waiting.base import PlaySpec
