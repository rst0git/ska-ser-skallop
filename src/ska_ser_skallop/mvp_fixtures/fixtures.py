"""Fixtures for managing ska MVP as the SUT.

In essence, fixtures allow for injecting a realised instance of input parameters to given pytest
functions. In addition to provisioning the object, the fixture functions also ensures the object
(and the SUT it represents) is in a particular state by setting it up and configuring it beforehand.
Similarly, fixtures are also responsible for tearing down the particular state it set up at the end
of the test.
"""
import logging
import os
from contextlib import ExitStack, contextmanager
from types import SimpleNamespace
from typing import Callable, Generator, Literal, Union

import pytest

from ska_ser_skallop.connectors import configuration as conf
from ska_ser_skallop.event_handling.builders import TransitChecking
from ska_ser_skallop.event_handling.logging import LogChecking
from ska_ser_skallop.mvp_control.describing.mvp_names import set_scope
from ska_ser_skallop.mvp_control.entry_points import configuration as entry_conf
from ska_ser_skallop.mvp_control.entry_points import testing
from ska_ser_skallop.mvp_control.entry_points import types as conf_types
from ska_ser_skallop.mvp_control.entry_points.base import EntryPoint
from ska_ser_skallop.mvp_control.infra_mon.configuration import (
    TangoBasedRelease,
    get_mvp_release,
)
from ska_ser_skallop.mvp_control.subarray.base import SBConfig
from ska_ser_skallop.mvp_fixtures.base import ExecSettings
from ska_ser_skallop.mvp_fixtures.context_management import (
    SubarrayContext,
    TelescopeContext,
)
from ska_ser_skallop.mvp_fixtures.env_handling import ExecEnv
from ska_ser_skallop.mvp_management import telescope_management as tel
from ska_ser_skallop.utils import env

from .context_monitoring import ContextMonitor
from .reflector import Reflector

logger = logging.getLogger(__name__)


class SubarrayAllocationSpec:
    """Object holding the high level resource allocation specifications."""

    def __init__(
        self,
        subarray_id=1,
        receptors=[1, 2],
        composition: Union[None, conf_types.Composition] = None,
    ) -> None:
        """Initialise the object.

        :param subarray_id: the index number to use for identifying the subarray, defaults to 1
        :param receptors: the receptors to uses indicated by index numbers, defaults to [1, 2]
        :param composition: the composition type to use, defaults to None
        """
        self.subarray_id = subarray_id
        self.receptors = receptors
        if not composition:
            composition = conf_types.Composition(conf_types.CompositionType.STANDARD)
        self.composition = composition


class SubarrayConfigurationSpec:
    """Object holding the high level scan configuration specifications."""

    def __init__(
        self,
        duration: float = 2.0,
        scan_config: Union[None, conf_types.ScanConfiguration] = None,
    ) -> None:
        """Initialise the object.

        :param duration: the scan duration time in seconds
        :param scan_config: the configuration type to use, defaults to None
        """
        self.duration = duration
        if not scan_config:
            scan_config = conf_types.ScanConfiguration(
                conf_types.ScanConfigurationType.STANDARD,
            )
        self.scan_config = scan_config


class fxt_types(SimpleNamespace):
    """Class holding the expected return types for fixtures.

    Use this to have a documented api available for a given fixture that you selected.

    E.g.:

    ..    code-block:: python

        def test(sb_config: fxt_types.sb_config):
            assert sb_config.sbid

    """

    sb_config = SBConfig
    set_exec_env = ExecEnv
    set_session_exec_env = ExecEnv
    session_exec_env = ExecEnv
    exec_env = ExecEnv
    entry_point = EntryPoint
    exec_settings = ExecSettings
    telescope_context = TelescopeContext
    running_telescope = TelescopeContext
    standby_telescope = TelescopeContext
    allocated_subarray = SubarrayContext
    configured_subarray = SubarrayContext
    mock_entry_point = testing.MockedEntryPoint
    context_monitoring = ContextMonitor
    transit_checking = TransitChecking
    log_checking = LogChecking
    infra_monitoring = TangoBasedRelease
    session_exec_settings = ExecSettings
    subarray_allocation_spec = SubarrayAllocationSpec
    subarray_configuration_spec = SubarrayConfigurationSpec
    set_subsystem_online = Callable[[EntryPoint], None]
    reflector = Reflector


@pytest.fixture(name="set_subsystem_online", scope="session")
def fxt_set_subsystem_online(
    session_exec_settings: ExecSettings,
) -> Callable[[EntryPoint], None]:
    """Fixture that is used to take a subsystem online using the given entrypoint.

    :param session_exec_settings: session wide exec settings
    :return: the callable function that takes an entrypoint and sets the relevant components
        online.
    """

    def fn_set_subsystem_online(entry_point: EntryPoint):
        with entry_conf.inject_entry_point(
            entry_point,
            "entry_point used for setting subsystem online",
        ):
            tel.set_offline_components_to_online(session_exec_settings)

    return fn_set_subsystem_online


@pytest.fixture(name="check_infra_per_session", autouse=True, scope="session")
def fxt_check_infra_per_session() -> Union[None, TangoBasedRelease]:
    """Set up fixture to automatically check infra per session.

    :return: The fixture
    """
    if os.getenv("CHECK_INFRA_PER_TEST") is None:
        if os.getenv("CHECK_INFRA_PER_SESSION"):
            logger.info("checking infra health before executing test session")
            release = get_mvp_release()
            assert release.devices_health == "READY"
            return release
    return None


@pytest.fixture(name="check_infra_per_test", autouse=True)
def fxt_check_infra_per_test(
    check_infra_per_session: Union[None, TangoBasedRelease]
) -> Union[None, TangoBasedRelease]:
    """Set a fixture to automatically check infra per test.

    :param check_infra_per_session: reference to session checking
    :return: The fixture
    """
    if os.getenv("CHECK_INFRA_PER_TEST"):
        logger.info("checking infra health before executing test")
        if check_infra_per_session:
            release = check_infra_per_session
        else:
            release = get_mvp_release()
        assert release.devices_health == "READY"
        return release
    return None


@pytest.fixture(name="reflector")
def fxt_reflector(
    telescope_context: TelescopeContext, exec_settings: ExecSettings
) -> Reflector:
    """Fixture for setting up a Reflector object.

    :param telescope_context: a reference to telescope context that is used by the object.
    :param exec_settings: a reference to settings object  that is used by the object.
    :return: A Reflector object instance
    """
    return Reflector(telescope_context, exec_settings)


@pytest.fixture(name="infra_monitoring")
def fxt_infra_monitoring(
    check_infra_per_test: Union[None, TangoBasedRelease]
) -> TangoBasedRelease:
    """Set a fixture to use for infra monitoring.

    :param check_infra_per_test: reference to test checking
    :return: The fixture
    """
    if check_infra_per_test:
        return check_infra_per_test
    return get_mvp_release()


@pytest.fixture(name="sb_config")
def fxt_sb_config():
    """Return a SBConfig instance."""
    # noqa: DAR101, DAR301, DAR201
    return SBConfig()


@pytest.fixture(name="context_monitoring")
def fxt_context_monitoring():
    """Construct a ContextMonitor object.

    :return: ContextMonitor
    """
    return ContextMonitor()


@pytest.fixture(name="transit_checking")
def fxt_transit_checking(context_monitoring: ContextMonitor):
    """Fixture used for injecting a TransitChecking object.

    :param context_monitoring: fixture for getting context_monitoring object.
    :return: a ContextMonitor
    """
    return TransitChecking(context_monitoring.builder)


@pytest.fixture(name="log_checking")
def fxt_log_checking(context_monitoring: fxt_types.context_monitoring):
    """Fixture used for injecting a LogChecking object.

    :param context_monitoring: fixture for getting context_monitoring object.
    :return: a ContextMonitor
    """
    log_checking = LogChecking()
    context_monitoring.add_logging_context(log_checking)
    return log_checking


@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item, call):
    """Run after test hook determining test outcome."""
    # noqa: DAR101, DAR301
    outcome = yield
    rep = outcome.get_result()

    setattr(item, "rep_" + rep.when, rep)


@pytest.fixture(name="set_session_exec_env", scope="session")
def fxt_set_session_exec_env() -> ExecEnv:
    """Fixture to inject a user specific session scoped exec env.

    If you want to set this
    then you must override it e.g.

    ..    code-block:: python

        @pytest.fixture(scope="session")
        def fxt_set_exec_env(set_session_exec_env: fxt_types.set_session_exec_env):
            exec_env.session_entry_point = "mock"

    :return: the session scoped exec env
    """
    logger.debug("setting general execution environment as ExecEnv object")
    return ExecEnv()


@pytest.fixture(name="set_exec_env")
def fxt_set_exec_env(set_session_exec_env: ExecEnv) -> Union[None, ExecEnv]:
    """Fixture to inject a user specific exec env.

    If you want to set this
    then you must override it e.g.

    ..    code-block:: python

        def fxt_set_exec_en(set_exec_env: fxt_types.set_exec_env):
            exec_env.entrypoint = "mock"

    :param set_session_exec_env: reference to settings that was set and used at start of session
    :return: [description]
    """
    return set_session_exec_env


def _populate_exec_env_settings(exec_env: ExecEnv, request):
    if exec_env.telescope_type is None:
        telescope_type = _determine_telescope_type(request)
        exec_env.telescope_type = telescope_type
    if exec_env.entrypoint is None:
        if os.getenv("MOCK_SUT"):
            logger.debug("set to use a mock entrypoint for test")
            exec_env.entrypoint = "mock"
        else:
            logger.debug("set to use a default tmc based entrypoint for test")
            exec_env.entrypoint = "tmc"
    if exec_env.session_entry_point is None:
        if os.getenv("MOCK_SUT"):
            logger.debug("set to use a mock entrypoint for test session")
            exec_env.session_entry_point = "mock"
        else:
            logger.debug("set to use the test entry point for session based control")
            exec_env.session_entry_point = exec_env.entrypoint
    if exec_env.maintain_on is None:
        if os.getenv("DISABLE_MAINTAIN_ON"):
            logger.debug(
                "set to disable telescope maintained ON during entire test session"
            )
            exec_env.maintain_on = False
        else:
            exec_env.maintain_on = True
    if exec_env.scope is None:
        if scope := os.getenv("SCOPE"):
            logger.debug(
                f"set to use a scope for a test given by an ENV variable: SCOPE={scope}"
            )
            exec_env.scope = [scope]
        else:
            exec_env.scope = []
    if exec_env.session_scope is None:
        if scope := os.getenv("SESSION_SCOPE"):
            logger.debug(
                f"set to use a scope for a session given by an ENV variable: SCOPE={scope}"
            )
            exec_env.session_scope = [scope]
        else:
            exec_env.session_scope = exec_env.scope


@pytest.fixture(name="session_exec_env", scope="session")
def fxt_session_exec_env(
    request,
    set_session_exec_env: ExecEnv,
) -> ExecEnv:
    """Inject the initial session scoped execution environment settings.

    :param request: A pytest request object
    :param set_session_exec_env: A exec env if it has been set by user
    :return: The object holding the telescope type and entry point information.
    """
    exec_env = set_session_exec_env
    _populate_exec_env_settings(exec_env, request)
    return exec_env


@pytest.fixture(name="exec_env")
def fxt_exec_env(request, set_exec_env: ExecEnv):
    """Determine the initial execution environment configuration from the host env settings.

    :param request: A pytest request object
    :param set_exec_env: A exec env if it has been set by user
    :return: The object holding the telescope type and entry point information.
    """
    exec_env = set_exec_env
    _populate_exec_env_settings(exec_env, request)
    return exec_env


@pytest.fixture(name="entry_point")
def fxt_entry_point() -> EntryPoint:
    """Generate and determine the appliceable entrypoint to use.

    :returns: the entry point
    """
    entry_point = entry_conf.get_entry_point()
    logger.debug(f"set to use entry point {entry_point}")
    return entry_point


@contextmanager
def _telescope_set_as(tel_type: Literal["skalow", "skamid"]):
    if tel_type == "skalow":
        logger.info("conducting tests for ska low")
        env.set_telescope_as_low()
        yield
    else:
        logger.info("conducting tests for ska mid")
        env.set_telescope_as_mid()
        yield


def _determine_telescope_type(request) -> Literal["skalow", "skamid"]:
    if request.scope == "session":
        # take the first markers from functions calling the session for the first time
        markernames = list(
            set(
                [
                    marker.name
                    for item in request.node.items
                    for marker in item.own_markers
                ]
            )
        )
    else:
        markernames = [marker.name for marker in request.node.own_markers]
    if not env.telescope_set_as_low_from_env():
        telescope_type_from_env = "skamid"
    else:
        telescope_type_from_env = "skalow"
    if all(["skamid" in markernames, "skalow" in markernames]):
        # if tests are aimed at both mid and low we look at the TEL env
        return telescope_type_from_env
    elif "skalow" in markernames:
        return "skalow"
    elif "skamid" in markernames:
        return "skamid"
    return telescope_type_from_env


@contextmanager
def _set_up_entry_point(
    env: ExecEnv, scope: Literal["session", "function"]
) -> Generator[None, None, None]:
    if scope == "session":
        assert env.session_entry_point
        entry_point = env.session_entry_point
    else:
        assert env.entrypoint
        entry_point = env.entrypoint
    if isinstance(entry_point, str):
        if entry_point == "mock":
            logger.warning(f"tests are performed on a mocked entry point per {scope}")
            entry_point = testing.get_mocked_entry_point()

            with entry_conf.inject_entry_point(
                entry_point,
                f"pytest {scope} fixture from exec env",
            ):
                with conf.patch_factory_for_testing() as factory:
                    entry_point.model.inject_factory(factory)
                    entry_point.activate_spy()
                    yield
        else:
            entry_point = entry_conf.determine_entry_point(entry_point)
            logger.debug(
                f"injecting an entrypoint class to be used based on string setting: {entry_point}"
            )
            with entry_conf.inject_entry_point(
                entry_point,
                f"pytest {scope} fixture from exec env",
            ):
                yield
    else:
        logger.debug(
            f"injecting an entrypoint class to be used based on fixture: {entry_point}"
        )
        with entry_conf.inject_entry_point(
            entry_point,
            f"pytest {scope} fixture from exec env",
        ):
            yield


@contextmanager
def _setup_env(
    exec_env: ExecEnv, scope: Literal["session", "function"]
) -> Generator[None, None, None]:
    if not env.is_telescope_type_already_set():
        env.set_telescope_type_from_env()
    if scope == "session":
        assert exec_env.session_scope is not None
        with set_scope(*exec_env.session_scope):
            with _set_up_entry_point(exec_env, scope):
                yield
    else:
        assert exec_env.scope is not None
        with set_scope(*exec_env.scope):
            with _set_up_entry_point(exec_env, scope):
                yield


@pytest.fixture(name="exec_settings")
def fxt_exec_settings(request) -> ExecSettings:
    """Generate an ExecSettings implementation.

    :param request: The request fixture injected by pytest
    :return: the ExecSettings implementation.
    """
    return ExecSettings(request.node)


@pytest.fixture(name="session_exec_settings", scope="session")
def fxt_session_exec_settings(request) -> ExecSettings:
    """Generate an ExecSettings implementation per session.

    :param request: The request fixture injected by pytest
    :return: the ExecSettings implementation.
    """
    return ExecSettings(request.node)


@pytest.fixture(name="telescope_context", scope="session")
def fxt_telescope_context(
    session_exec_env: ExecEnv, session_exec_settings: ExecSettings
):
    """Generate a telescope context within which it must be maintained.

    :param session_exec_settings: Injected exec_settings set by a fixture
    :param session_exec_env: the exec env containing user settings fore entire scope
    :yield: The telescope context object.
    """
    if session_exec_env.maintain_on:
        logger.info("setting telescope to maintain being on for the entire session")
        with _setup_env(session_exec_env, "session"):
            with ExitStack() as session_stack:
                context = TelescopeContext(session_stack, session_exec_settings)
                context.set_up_a_telescope_per_session()
                yield context
    else:
        logger.info("telescope will be switched on/off for each test")
        with _setup_env(session_exec_env, "session"):
            with ExitStack() as session_stack:
                context = TelescopeContext(session_stack, session_exec_settings)
                yield context


@pytest.fixture(name="running_telescope")
def fxt_running_telescope(
    telescope_context: "TelescopeContext",
    exec_settings: ExecSettings,
    exec_env: ExecEnv,
):
    """Fixture used to set a telescope into a running (ON) state.

    Note that after test has finished the telescope will be switched OFF at the end
    of the test session unless the fixture overriding maintain_on has been set to
    return False.

    :param telescope_context: A telescope context object to control the telescope with.
    :param exec_settings: fixture containing default execution settings
    :param exec_env: the exec env containing user settings
    :yield: The telescope context object in the ON state
    """
    assert telescope_context
    telescope_context.clear_flags()
    telescope_context.set_exec_settings(exec_settings)
    if exec_env.maintain_on:
        logger.debug("setting telescope operation at the start of the test session")
        with telescope_context.maintain_telescope_to_running(exec_settings):
            with _setup_env(exec_env, "function"):
                with ExitStack() as stack:
                    telescope_context.set_test_stack(stack)
                    yield telescope_context
    else:
        with _setup_env(exec_env, "function"):
            with ExitStack() as stack:
                telescope_context.set_test_stack(stack)
                logger.debug("setting telescope operation at the start of the test")
                telescope_context.set_up_a_telescope(exec_settings)
                yield telescope_context


@pytest.fixture(name="standby_telescope")
def fxt_standby_telescope(telescope_context: "TelescopeContext", exec_env: ExecEnv):
    """Fixture used to get a telescope in the OFF state.

    Note that after test has finished the telescope will be switched ON at the end
    of the test session unless the fixture overriding maintain_on has been set to
    return False.

    :param exec_env: the exec env containing user settings
    :param telescope_context: The initialised telescope context from a fixture
    :yield telescope_context: TelescopeContext
    """
    assert telescope_context
    telescope_context.clear_flags()
    if exec_env.maintain_on:
        with _setup_env(exec_env, "function"):
            with ExitStack() as stack:
                telescope_context.set_test_stack(stack)
                logger.debug(
                    "setting telescope down (un-operational) at the start of the test"
                    " (will be set to operational at the end of the test)"
                )
                telescope_context.set_down_a_telescope()
                yield telescope_context
    else:
        with _setup_env(exec_env, "function"):
            with ExitStack() as stack:
                telescope_context.set_test_stack(stack)
                logger.debug(
                    "telescope will be switched off at the end of test if left ON"
                )
                telescope_context.switch_off_after_test()
                yield telescope_context


@pytest.fixture(name="subarray_allocation_spec")
def fxt_subarray_allocation_spec(tmp_path) -> SubarrayAllocationSpec:
    """Set up a default subarray allocation spec that can be interjected by a tester.

    :param tmp_path: temporary file location for defining a composition
    :returns: The specification as a fixture.
    """
    subarray_id = 1
    receptors = [1, 2]
    composition = conf_types.CompositionByFile(
        tmp_path, conf_types.CompositionType.STANDARD
    )
    return SubarrayAllocationSpec(subarray_id, receptors, composition)


@pytest.fixture(name="allocated_subarray")
def fxt_allocated_subarray(
    running_telescope: "TelescopeContext",
    subarray_allocation_spec: SubarrayAllocationSpec,
    sb_config,
    exec_settings: ExecSettings,
):
    """Generate a subarray context object in the IDLE state with a set of resources assigned.

    Note the subarray that will be allocated will be have a static configuration as follows:

        #. Subarray id = 1
        #. receptors to use will be receptors 1 and 2
        #. Configuration of sdp and csp will be a standard type conf_types.CompositionType.STANDARD

    :param running_telescope: The telescope in ON state as provided by a fixture.
    :param subarray_allocation_spec: Fixture containing the high level specifications for allocating
        a subarray.
    :param sb_config: A sb configuration containing context data relevant to the SB.
    :param exec_settings: Execution settings to use during I/O calls.
    :return: A subarray context object in the IDLE state.
    """
    subarray_id = subarray_allocation_spec.subarray_id
    receptors = subarray_allocation_spec.receptors
    composition = subarray_allocation_spec.composition
    logger.debug(
        "Setting up a subarray as part of fixture using the injected entry point:"
        f"subarray_id: {subarray_id}"
        f", receptors: {receptors}"
        f", composition: {composition}."
        "Note, subarray will be released automatically at the end of test."
    )
    return running_telescope.allocate_a_subarray(
        subarray_id,
        receptors,
        sb_config,
        exec_settings,
        composition=composition,
    )


@pytest.fixture(name="subarray_configuration_spec")
def fxt_subarray_configuration_spec(tmp_path) -> SubarrayConfigurationSpec:
    """Set up a default scan configuration spec that can be interjected by a tester.

    :param tmp_path: temporary file location for defining a composition
    :return: the configuration specification
    """
    duration = 2.0
    scan_config = conf_types.ScanConfigurationByFile(
        tmp_path,
        conf_types.ScanConfigurationType.STANDARD,
    )
    return SubarrayConfigurationSpec(duration, scan_config)


@pytest.fixture(name="configured_subarray")
def fxt_configured_subarray(
    allocated_subarray: SubarrayContext,
    subarray_configuration_spec: SubarrayConfigurationSpec,
    exec_settings: ExecSettings,
):
    """Return a configured subarray based on an existing subarray instance created by a fixture.

    :param allocated_subarray: The subarray that is already allocated as a fixture.
    :param subarray_configuration_spec: The scan configuration spec.
    :param exec_settings: Execution settings to use during I/O calls.
    :return: A subarray context object in the IDLE state.
    """
    duration = subarray_configuration_spec.duration
    scan_config = subarray_configuration_spec.scan_config
    logger.debug(
        "Configuring a subarray for a scan as part of fixture using the injected entry point:"
        f"duration: {duration}"
        f"scan_config: {scan_config}"
        "Note, subarray configuration will be cleared at the end of test."
    )
    allocated_subarray.configure(scan_config, duration, exec_settings)
    return allocated_subarray


@pytest.fixture(name="mock_entry_point")
def fxt_mock_entry_point() -> testing.MockedEntryPoint:
    """Provide a reference to singleton mocked entry point during dev testing.

    :return: The mocked entry point

    """
    return testing.get_new_mocked_entry_point()
