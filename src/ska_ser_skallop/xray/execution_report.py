import json
from datetime import datetime
from pathlib import Path
from typing import List, Set, Tuple, TypedDict, Union

from ska_ser_skallop.utils.dictionary_validation import validated_typed_dict


class DuratedDict(TypedDict):
    duration: float
    created: float


class SummaryDict(TypedDict):
    passed: Union[int, None]
    total: int
    collected: int


class TestResult(TypedDict):
    nodeid: str
    lineno: int
    outcome: str
    keywords: List[str]


@validated_typed_dict
class ReportData(DuratedDict):
    summary: SummaryDict
    tests: List[TestResult]


def get_report_data(path: Path) -> ReportData:
    data = json.loads(path.read_text())
    return ReportData(**data)


def get_keywords(report_data: ReportData) -> Set[str]:
    keywords = set()
    for test in report_data["tests"]:
        for keyword in test["keywords"]:
            keywords.add(keyword)
    return keywords


def format_dates(start_date: datetime, end_date: datetime):
    start_date_formatted = f"{start_date.isoformat()[:-3]}+0000"
    end_date_formatted = f"{end_date.isoformat()[:-3]}+0000"
    return start_date_formatted, end_date_formatted


def get_dates(data: DuratedDict) -> Tuple[datetime, datetime]:
    start_time_stamp = data["created"]
    end_time_stamp = data["created"] + data["duration"]
    start_date = datetime.fromtimestamp(start_time_stamp)
    end_date = datetime.fromtimestamp(end_time_stamp)
    return start_date, end_date
