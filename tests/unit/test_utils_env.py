from pathlib import Path

from ska_ser_skallop.utils.env import get_temp_dir


def test_get_temp_dir():
    dir = get_temp_dir()
    path = Path(dir)
    assert path.exists()
