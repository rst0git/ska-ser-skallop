"""Testing module that tests event waiting"""
from typing import Any, NamedTuple, Set

import mock
import pytest

from ska_ser_skallop.mvp_control.event_waiting import wait as sut

LIB = "ska_ser_skallop.event_waiting.wait"


class Context(NamedTuple):
    builder: sut.MessageBoardBuilder
    timeout: int
    log_enabled: bool
    observe_only: bool
    spec: Set[Any]


@pytest.fixture(name="context")
def fxt_context():
    with mock.patch(f"{LIB}.MessageBoardBuilder") as builder:
        yield Context(builder, 1, False, False, set())
