from unittest import mock

import pytest

from ska_ser_skallop.connectors import configuration as conf
from ska_ser_skallop.connectors.mockingfactory import MockingFactory
from ska_ser_skallop.mvp_control.rectifying.rectifier import Rectifier


@pytest.fixture(name="mocked_device_proxy")
def fxt_test_factory():
    testing_factory = MockingFactory()
    with conf.patch_factory(testing_factory, "test fixture"):
        mocked_device_proxy = testing_factory.set_device_spy()
        yield mocked_device_proxy


def test_rectifier(mocked_device_proxy, caplog):
    rectifier = Rectifier()
    mocked_device_proxy.name.return_value = "device_name"
    rectifier.switch_on(["device_name"])
    assert caplog.messages
    assert caplog.messages[0] == "['device_name'] have been switched ON"

    caplog.clear()
    rectifier.switch_off(["mid-csp/control/0"])
    assert caplog.messages
    assert caplog.messages[0] == "['mid-csp/control/0'] have been switched OFF"

    caplog.clear()
    with mock.patch("ska_ser_skallop.mvp_control.rectifying.rectifier.atomic"):
        rectifier.restart_faulty_subarrays(["device_name"])
        assert caplog.messages
        assert caplog.messages[0] == "device_name have been restarted"

    caplog.clear()
    with mock.patch("ska_ser_skallop.mvp_control.rectifying.rectifier.atomic"):
        rectifier.abort_and_restart_subarrays(["device_name"])
        assert caplog.messages
        assert caplog.messages[0] == "device_name have been aborted"

    caplog.clear()
    rectifier.rectify_faulty_devices(["device_name"], "ON")
    assert caplog.messages
    assert caplog.messages[0] == "device_name have been reset, will switch it ON"

    caplog.clear()
    rectifier.switch_off_resources(["device_name"])
    assert caplog.messages
    assert caplog.messages[0] == "['device_name'] have been switched OFF"

    caplog.clear()
    rectifier.switch_on_resources(["device_name"])
    assert caplog.messages
    assert caplog.messages[0] == "['device_name'] have been switched on"

    caplog.clear()
    with mock.patch("ska_ser_skallop.mvp_control.rectifying.rectifier.atomic"):
        rectifier.switch_off_dish_masters(["device_name"])
        assert caplog.messages
        assert caplog.messages[0] == "['device_name'] have been switched OFF"

    caplog.clear()
    with mock.patch("ska_ser_skallop.mvp_control.rectifying.rectifier.atomic"):
        rectifier.switch_on_dish_masters(["device_name"])
        assert caplog.messages
        assert caplog.messages[0] == "['device_name'] have been switched ON"
