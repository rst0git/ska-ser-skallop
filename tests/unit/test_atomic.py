from typing import List

import mock
import pytest

from ska_ser_skallop.subscribing.base import AttributeInt
from ska_ser_skallop.transactions.atomic import atomic

lib = "ska_ser_skallop.transactions.atomic"


class Subscriber:
    def push_event(self, event):
        pass


class Producer:
    def __init__(self, name="dummy") -> None:
        self.subscribers = []
        self.attr = None
        self.cb = None
        self.name = name
        self.attr_value = None

    def subscribe_event(self, attr, EventType, cb):
        self.attr = attr
        self.cb = cb
        return 0

    def unsubscribe_event(self, id):
        pass

    def push_event(self, event):
        self.attr_value = event
        self.cb(MockEvent(event, self.name))

    def read_attribute(self, _):
        if self.attr_value and self.attr:
            return AttributeInt(self.attr, self.attr_value)
        return None


class MockEvent:
    def __init__(self, value, name="dummy") -> None:
        self.value = value
        self.device = mock.Mock()
        self.device.name.return_value = name
        self.attr_value = mock.Mock()
        self.attr_value.name = "dummy"
        self.attr_value.value = value


@pytest.fixture(name="mock_device")
def fxt_mock_device() -> Producer:
    with mock.patch(f"{lib}.configuration") as mock_configuration:
        mock_device = Producer()
        mock_configuration.get_device_proxy.return_value = mock_device
        yield mock_device


@pytest.fixture(name="mock_devices")
def fxt_mock_devices():
    with mock.patch(f"{lib}.configuration") as mock_configuration:
        mock_devices = [Producer("dummy1"), Producer("dummy2")]
        mock_configuration.get_device_proxy.side_effect = mock_devices
        yield mock_devices


def test_wait_for_dummy(mock_device: Producer):
    with atomic("dummy", "attr", "event"):
        mock_device.push_event("first event")
        mock_device.push_event("event")


def test_wait_for_dummies(mock_devices: List[Producer]):
    mock_device1 = mock_devices[0]
    mock_device2 = mock_devices[1]
    with atomic(["dummy1", "dummy2"], "attr", "event"):
        mock_device1.push_event("first event")
        mock_device1.push_event("event")
        mock_device2.push_event("first event")
        mock_device2.push_event("event")
