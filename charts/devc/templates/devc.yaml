{{- if eq .Values.type "Job"}}
apiVersion: batch/v1
kind: Job
{{- else }}
apiVersion: apps/v1
kind: {{ .Values.type }}
{{- end }}
metadata:
  labels: {{ include "devc.labels" . }}
  name: {{ .Release.Name }}
spec:
  {{- if ne .Values.type "Job" }}
  selector:
    matchLabels: {{ include "devc.labels" . | indent 2 }}
      component: devpod
    {{- if eq .Values.type "StatefulSet" }}
  serviceName: {{ .Release.Name }}
    {{- end }}
  {{- end }}
  template:
    metadata:
      labels: {{ include "devc.labels" . | indent 4}}
        component: devpod
      name: {{ .Release.Name }}
    spec:
      {{- if eq .Values.type "job"}}
      restartPolicy: Never
      {{- end }}
      containers:
      - name: {{ .Release.Name }}
        image: {{ .Values.image }}
        command: {{ include "devc.entrypoint" . | indent 4 }}
        env:
        - name: VIRTUAL_ENV
          value: /app/venv
        - name: SKUID_URL
          value: {{ include "devc.skuidurl" . }}
        - name: PATH
          value: /app/venv/bin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/sbin:/bin
        {{- if .Values.env }}
        {{- include "devc.env" . | indent 4 }}
        {{- end }}
        {{- if .Values.tango }}
        - name: TANGO_HOST
          value: {{ include "devc.tangohost" . }}
        {{- end }}
        volumeMounts:
        {{- if .Values.persistantWorkspace.enabled }}
        - name: workspace
          mountPath: /app/workspace
        {{- end }}
        {{- if .Values.vscode.enabled }}
        - name: vscode-settings
          mountPath: /.vscode
        {{- end }}
        {{- if .Values.install.enabled }}
        - name: pythenv
          mountPath: /app/venv
        {{- end }}
        resources:
        {{- toYaml .Values.resources | nindent 10 }}
      initContainers:
      {{- if .Values.tango.deviceServer.enabled }}
      - name: register
        image: artefact.skao.int/ska-tango-images-pytango-builder:9.3.10
        command: {{ include "devc.register" .| indent 4 }}
        env:
        - name: TANGO_HOST
          value: {{ include "devc.tangohost" . }}
      {{- end }}
      {{- if .Values.install.enabled }}
      - name: install
        image: artefact.skao.int/ska-tango-images-pytango-builder:9.3.10
        command: {{ include "devc.install" .| indent 4 }}
        volumeMounts:
        - name: pythenv
          mountPath: /app/
        {{- if .Values.requirements }}
        - name: requirements
          mountPath: /req
        {{- end }}
      {{- end }}
      volumes:
      {{- if .Values.install.enabled }}
      - name: pythenv
        emptyDir: {}
      {{- end }}
      {{- if .Values.vscode.enabled }}
      - name: vscode-settings
        configMap:
          name: {{ .Release.Name }}-vscode-settings
      {{- end }}
      {{- if .Values.persistantWorkspace.enabled }}
      - name: workspace
        persistentVolumeClaim:
          claimName: {{ .Values.persistantWorkspace.pvcName }}
      {{- end }}
      {{- if .Values.requirements }}
      - name: requirements
        configMap:
          name: {{ .Release.Name }}-requirements
      {{- end }}
---
{{- if .Values.requirements }}
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ .Release.Name }}-requirements
  labels: {{ include "devc.labels" . }}
data:
  requirements.txt: |-
  {{- include "devc.requirements" . | indent 2 }}
---
{{- end }}
{{- if .Values.vscode.enabled }}
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ .Release.Name }}-vscode-settings
  labels: {{ include "devc.labels" . }}
data:
  .env: |-
    {{- range .Values.vscode.env }}
    {{ . }}
    {{- end }}
    TANGO_HOST={{ include "devc.tangohost" . }}
    SKUID_URL={{ include "devc.skuidurl" . }}
    PYTHONPATH=/app/{{ .Values.install.project }}/{{ .Values.vscode.path }}
  settings.json: |-
    {{- .Files.Get "data/settings.json" | nindent 4 -}}
{{- end }}

