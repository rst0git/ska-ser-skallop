{{/* vim: set filetype=mustache: */}}
{{/*
Create a install command from the provided values
*/}}
{{- define "testjob.install" }}
    - bash
    - -c
    {{- if .Values.requirements }}
    - "pip3 install virtualenv && virtualenv venv && source venv/bin/activate && git clone {{ .Values.gitRepo }}/{{ .Values.project }}.git  && pip3 install skallop=={{ .Values.ver }} -r /req/requirements.txt --extra-index-url {{ .Values.extraRepo }}"
    {{- else }}
    - "pip3 install virtualenv && virtualenv venv && source venv/bin/activate && git clone {{ .Values.gitRepo }}/{{ .Values.project }}.git  && pip3 install skallop=={{ .Values.ver }} --extra-index-url {{ .Values.extraRepo }}"
    {{- end }}
{{- end }}
{{/*
Create env vars
*/}}
{{- define "testjob.env" }}
  {{- range .Values.env }}
    - name: {{ .name }}
      value: {{ .value }}
  {{- end }}
{{- end }}
{{/*
Create requirements file from values
*/}}
{{- define "testjob.requirements"}}
    {{- range .Values.requirements}}
    {{ . }}
    {{- end }}
{{- end }}
{{/*
Create spec file from values
*/}}
{{- define "testjob.specfile" }}
    group:
      runs: {{ .Values.spec.runs}}
      path: "{{ .Values.project }}/{{ .Values.spec.path}}/"
      modules:
      {{- range .Values.spec.modules }}
        - path: {{ .path}}
          tests:
        {{- range .tests }}
            - name: {{ .name }}
        {{- end }}
      {{- end }}
{{- end }}




