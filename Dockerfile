FROM registry.gitlab.com/ska-telescope/ska-ser-skallop/base:2.0.3 as build

COPY . .

RUN make python-do-build \
    && cp -rf dist /dist

FROM registry.gitlab.com/ska-telescope/ska-skampi/ska-skampi-ci-base:0.1.0

# install skallop latest as part of site dependencies
COPY --from=build dist /tmp/dist

RUN pip3 install /tmp/dist/*.whl && \
    pip3 install virtualenv

COPY --from=bitnami/kubectl:latest /opt/bitnami/kubectl/bin/kubectl /usr/local/bin/kubectl

COPY --from=alpine/helm:latest /usr/bin/helm /usr/local/bin/helm

RUN curl -sSL https://install.python-poetry.org | python3  - --preview

ENV PATH /root/.local/bin:${PATH}

RUN virtualenv .venv

RUN poetry config virtualenvs.in-project true

RUN echo "cp -R /usr/local/lib/python3.9/dist-packages/pytango-9.3.3.dist-info .venv/lib/python3.9/site-packages/pytango-9.3.3.dist-info" > /root/.local/bin/getpytango.sh 