# This Makefile uses templates defined in the .make/ folder, which is a git submodule of
# https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile.

-include .make/docs.mk
-include .make/help.mk
-include .make/oci.mk
-include .make/python.mk
-include .make/release.mk

-include develop.mk
# TODO: Including previous Makefile here until we can figure out what targets need to be
# preserved.

PYTHON_RUNNER:= .venv/bin/python -m
PYTHON_LINT_TARGET:=src/ tests/ resources/examples/
PYTHON_PUBLISH_URL:=https://artefact.skao.int/repository/pypi-internal/
PYTHON_SWITCHES_FOR_BLACK :=
PYTHON_SWITCHES_FOR_ISORT :=

.PHONY: live-reload update_doc_requirements check-format

.PHONY: live-reload update_doc_requirements

python-install: poetry.lock
	poetry config virtualenvs.in-project true
	poetry install --with tests,dev,docs --sync

python-do-build:
	poetry build

python-do-publish:
	poetry publish --repository skao --username $(PYTHON_PUBLISH_USERNAME) --password $(PYTHON_PUBLISH_PASSWORD)


update_doc_requirements: docs/requirements.txt

docs/requirements.txt: 
	poetry show --with docs | awk '{print $$1"=="$$2}' | sed '/.*!./d' > docs/requirements.txt

live-reload:
	sphinx-autobuild docs/src docs/_build/html

check-format:
	poetry run python3 -m isort --check-only --profile black src/ tests/ resources/examples/
