read -e -p "Name of local branch (or staging/staging-low):" -i "integration" branch
if [ "$branch" = "integration" ] ; then
    export DOMAIN='integration'
    export KUBE_BRANCH='integration'
    export KUBE_HOST=k8s.skao.stfc
elif [ "$branch" = "staging" ] ; then
    export DOMAIN='staging'
    export KUBE_BRANCH='staging'
    export KUBE_HOST=k8s.skao.stfc
elif [ "$branch" = "staging-low" ]; then
    export TANGO_BRIDGE_IP='psi-head.atnf.csiro.au'
    export DOMAIN='staging-low'
    export KUBE_BRANCH='staging-low'
    export KUBE_HOST=$TANGO_BRIDGE_IP
    TEL='low'
else
    export DOMAIN='branch'
    export KUBE_BRANCH=$branch
    export KUBE_HOST=k8s.skao.stfc
fi
if [ "$branch" != "staging-low" ]; then
    read -e -p "Telescope variant:" -i "mid" variant
    export TEL=$variant
fi
export KUBE_NAMESPACE='ci-ska-skampi-'$branch'-'$TEL
read -e -p "taranta user name:"  username
export TARANTA_USER=$username
read -e -p "Taranta password:"  password
export TARANTA_PASSWORD=$password
export TEST_ENV='BUILD_OUT'
read -e -p "maintain telescope operational during entire test session?:" -i "yes" maintain_on
if [ "$maintain_on" = "yes" ] ; then
    unset DISABLE_MAINTAIN_ON
else
    export DISABLE_MAINTAIN_ON="True"
fi
echo "env variables set: 
DOMAIN=$DOMAIN
KUBE_BRANCH=$KUBE_BRANCH
TEL=$TEL
TARANTA_USER=$TARANTA_USER
TARANTA_PASSWORD=$TARANTA_PASSWORD
TEST_ENV=$TEST_ENV
DISABLE_MAINTAIN_ON=$DISABLE_MAINTAIN_ON
KUBE_BRANCH=$KUBE_BRANCH
KUBE_NAMESPACE=$KUBE_NAMESPACE"
if [ "$branch" = "staging-low" ]; then
    echo "TANGO_BRIDGE_IP='psi-head.atnf.csiro.au'"
fi
