import pytest

from ska_ser_skallop.connectors import configuration
from ska_ser_skallop.mvp_control.describing import mvp_names


@pytest.fixture(name="maintain_on", scope="session", autouse=True)
def fxt_override_maintain_on():
    return True


@pytest.mark.skamid
@pytest.mark.skalow
@pytest.mark.usefixtures("running_telescope")
def test_that_relies_on_running_telescope():
    # fxt_running_telescope is used to put the telescope into running state it is scoped
    # to remain the the running state for the entire session
    # central_node_name = mvp_names.Mid.tm.central_node
    # central_node = configuration.get_device_proxy(central_node_name)
    # assert central_node.State().name == "ON"  # type: ignore
    pass


def test_that_telescope_remains_in_running():
    # fxt_running_telescope is used to put the telescope into running state it is scoped
    # to remain the the running state for the entire session
    subarray_node_name = mvp_names.Mid.tm.subarray(1).__str__()
    central_node = configuration.get_device_proxy(subarray_node_name)
    assert central_node.State().name == "ON"  # type: ignore
