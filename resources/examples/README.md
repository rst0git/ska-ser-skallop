## Examples

### Using the `running_telescope` fixture

```python
import pytest

from ska_ser_skallop.connectors import configuration
from ska_ser_skallop.mvp_control.describing import mvp_names


# fxt to override setting that makes running telescope be switched on/of for each test
# setting it to True will cause the telescope to be maintained on for the entire test session
@pytest.fixture(name="maintain_on", scope="session", autouse=True)
def fxt_override_maintain_on():
    return True

@pytest.mark.usefixtures('running_telescope')
def test_that_relies_on_running_telescope():
   
    central_node_name = mvp_names.Mid.tm.central_node
    central_node = configuration.get_device_proxy(central_node_name)
    assert central_node.State() == "ON" # type: ignore

def test_that_telescope_remains_in_running():
    # fxt_running_telescope is used to put the telescope into running state it is scoped
    # to remain the the running state for the entire session
    subarray_node_name = mvp_names.Mid.tm.subarray(1).__str__()
    central_node = configuration.get_device_proxy(subarray_node_name)
    assert central_node.State() == "ON" # type: ignore

```


### Using the `allocated_subarray` fixture
```python
import logging

import pytest

from ska_ser_skallop.connectors import configuration
from ska_ser_skallop.mvp_fixtures.context_management import TelescopeContext
from ska_ser_skallop.mvp_control.describing import mvp_names
from ska_ser_skallop.mvp_control.entry_points import types as conf_types

logger = logging.getLogger(__name__)

# fxt to override setting that makes running telescope be switched on/of for each test
# setting it to True will cause the telescope to be maintained on for the entire test session
@pytest.fixture(name="maintain_on", scope="session", autouse=True)
def fxt_override_maintain_on():
    return True


@pytest.mark.usefixtures('allocated_subarray')
def test_subarray_node_is_in_idle():
    # allocated subarray uses default two dishes and a 'standard' resource allocation
    subarray_node_name = mvp_names.Mid.tm.subarray(1).__str__()
    subarray_node = configuration.get_device_proxy(subarray_node_name)
    assert subarray_node.obsState.name == "IDLE" # type: ignore


def test_subarray_can_be_directly_created(running_telescope: TelescopeContext, tmp_path, sb_config):

    # use running telescope to create a new allocated subarray
    subarray_id = 1
    receptors = [1, 2, 4, 4]
    composition = conf_types.CompositionByFile(tmp_path, conf_types.CompositionType.STANDARD)

    subarray = running_telescope.allocate_a_subarray(
        subarray_id, receptors, sb_config, composition=composition
    )

    subarray_node_name = mvp_names.Mid.tm.subarray(1).__str__()
    subarray_node = configuration.get_device_proxy(subarray_node_name)
    assert subarray_node.obsState.name == "IDLE" # type: ignore
    assert subarray.id == 1

```


### Using the `configured_subarray` fixture

```python
import logging

import pytest

from ska_ser_skallop.connectors import configuration
from ska_ser_skallop.mvp_fixtures.context_management import TelescopeContext
from ska_ser_skallop.mvp_control.describing import mvp_names
from ska_ser_skallop.mvp_control.entry_points import types as conf_types

logger = logging.getLogger(__name__)

# fxt to override setting that makes running telescope be switched on/of for each test
# setting it to True will cause the telescope to be maintained on for the entire test session
@pytest.fixture(name="maintain_on", scope="session", autouse=True)
def fxt_override_maintain_on():
    return True

@pytest.mark.usefixtures('configured_subarray')
def test_subarray_node_is_in_ready():
    # configured_subarray subarray uses standard subarray of two dishes and a 'standard' resource allocation
    # plus a standard scan configuration
    subarray_node_name = mvp_names.Mid.tm.subarray(1).__str__()
    subarray_node = configuration.get_device_proxy(subarray_node_name)
    assert subarray_node.obsState.name == "READY" # type: ignore


def test_subarray_configuration_can_be_directly_created(running_telescope: TelescopeContext, tmp_path, sb_config):

    # use allocated_subarray to configure it directly
    subarray_id = 1
    receptors = [1, 2, 4, 4]
    composition = conf_types.CompositionByFile(tmp_path, conf_types.CompositionType.STANDARD)

    subarray = running_telescope.allocate_a_subarray(
        subarray_id, receptors, sb_config, composition=composition
    )

    duration = 2.0
    scan_config = conf_types.ScanConfigurationByFile(
        tmp_path,
        conf_types.ScanConfigurationType.STANDARD,
    )
       
    subarray.configure(scan_config, duration)

    subarray_node_name = mvp_names.Mid.tm.subarray(1).__str__()
    subarray_node = configuration.get_device_proxy(subarray_node_name)
    assert subarray_node.obsState.name == "READY" # type: ignore

```

