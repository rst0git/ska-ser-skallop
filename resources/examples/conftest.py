import pytest


# uncomment this if you want to override the default timeout settings in case your
# environment entails very long delays
@pytest.fixture(autouse=True)
def override_timeouts(exec_settings):
    exec_settings.time_out = 100
