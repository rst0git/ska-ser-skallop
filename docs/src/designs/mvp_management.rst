MVP Management
==============
As illustrated in the dependency diagram below, the mvp_management modules are a set of wrapper and facade
objects that rely on the mvp_control package for executing the control and monitoring tasks.

.. image:: /images/mvp_management.jpg
    :width: 100%

In particular the mvp_management package relies on the failure handling modules for each controller object to attempt
to rectify unhealthy states.

Note the reliance of telescope_management on the failure handlings of "sub systems" also.
