Fixtures
========
The conceptual design of the fixtures is illustrated in the diagram below.

.. image:: /images/fixture_design.jpg
    :width: 100%

The skallop fixtures are essentially focused on providing and setting the state of the following two
objects:

#. :py:class:`~ska_ser_skallop.mvp_fixtures.context_management.TelescopeContext`
#. :py:class:`~ska_ser_skallop.mvp_fixtures.context_management.SubarrayContext`

These two classes manipulate the state and sets correct tear downs for pytest using a set of
context manager functions belonging to the mvp_management package. These context managers in turn,
make use of the mvp_control package to correctly control and monitor readiness of the mvp during setup and tear down.

The corresponding object and it's required state for each providing fixture is listed below.

.. list-table::
    :header-rows: 1

    * - Fixture Name
      - Provides
      - Resulting State
    * - running_telescope
      - :py:class:`~ska_ser_skallop.mvp_fixtures.context_management.TelescopeContext`
      - ON
    * - standby_telescope
      - :py:class:`~ska_ser_skallop.mvp_fixtures.context_management.TelescopeContext`
      - OFF
    * - allocated_subarray
      - :py:class:`~ska_ser_skallop.mvp_fixtures.context_management.SubarrayContext`
      - IDLE
    * - configured_subarray
      - :py:class:`~ska_ser_skallop.mvp_fixtures.context_management.SubarrayContext`
      - READY
