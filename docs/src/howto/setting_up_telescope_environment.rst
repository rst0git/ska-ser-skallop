Configuring the telescope type
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The skallop package automatically selects the correct type of telescope based on user defined configuration.
To set this from a host env variable, either one of the following variable names must be used:

.. list-table::
    :header-rows: 1

    * - Var Name
      - Value meaning SKA Low
      - Value meaning SKA Mid
    * - SKA_TELESCOPE
      - SKA-low
      - SKA-Mid
    * - TEL
      - low
      - mid

Note that the env var `TEL` will be set when you setup remote connections using the `setenv.sh`
command (see :ref:`set-env-var-for-remote-connections`. )

.. todo::

  Section describing how to change scope of readiness checks once merge for feature into master done.
