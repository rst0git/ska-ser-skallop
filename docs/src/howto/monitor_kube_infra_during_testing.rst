How to monitor kubernetes cluster health during testing
=======================================================
This section describes how to get infrastructure related information in related to
deployed tango devices within pods and other resource related information.

When to use it
--------------
When particular tests have a possible adverse affect on the deployments and you need to detect and prevent
unhealthy deployments from causing downstream failures.

How to use the feature
----------------------
The front end object to use is provided as a fixture (that injects an
:py:class:`~ska_ser_skallop.mvp_control.infra_mon.tango_infra.TangoBasedRelease` object coming from the
:py:mod:`~ska_ser_skallop.mvp_control.infra_mon` module. 
This object provides a :py:meth:`~ska_ser_skallop.mvp_control.infra_mon.tango_infra.TangoBasedRelease.devices_health` property
returning a string Literal value of "NOT_READY", "READY" or "ERROR" as an aggregate of all devices running withing the current
release. 

If you are interested in the status of a specific device you can lookup a device
using the :py:meth:`~ska_ser_skallop.mvp_control.infra_mon.tango_infra.TangoBasedRelease.devices` property which will 
in turn return a :py:class:`~ska_ser_skallop.mvp_control.infra_mon.tango_infra.Device` object that will return it's deployment
status when you call :py:meth:`~ska_ser_skallop.mvp_control.infra_mon.tango_infra.Device.get_device_deployment_status`.

The example below gives an example of testing this in the form of a fixtures used by the test.


    .. code-block:: python

        @pytest.fixure(name='verify_device_deployed')
        def fxt_verify_device_deployed(infra_monitoring: fxt_types.infra_monitoring) -> bool:
            if device := infra_monitoring.devices.get("my_device_FQDN"):
                assert_that(device.get_device_deployment_status()).is_equal_to("Running")
                yield True
                assert_that(device.get_device_deployment_status()).is_equal_to("Running")
            else:
                yield False
            
        test_my_device(verify_device_deployed: bool):
            assert(verify_device_deployed)
            run_some_stuff_on_my_device_and_verify()
        