How to use MVP management
=========================

When to use it
--------------

When the SUT can conceptually be defined as a telescope with resources that, when grouped together
into a subarray, allows for performing configured scan as an atomic observation task.

The MVP management package assist a tester in ensuring a robust and correctly defined context, running
tests on configured subarrays, whilst taking care of general health and monitoring; readiness checks (before and after
SUT commands) and automatic setup and tear down of contexts.

What you need before starting
-----------------------------
#. Skallop package installed. The package will automatically install these fixtures.

Setting up your environment
---------------------------
The MVP management package defines the SUT in generic terms using abstraction for objects related to
telescope and subarrays. In order to set the concrete types the tester needs to configure the telescope type.

.. include:: setting_up_telescope_environment.rst

Using MVP management
--------------------

The example below illustrates the different context managers that can be used by each module and how they fit into each
other.


.. code-block:: python

    from ska_ser_skallop.mvp_management import types
    from ska_ser_skallop.mvp_management.subarray_composition import (
        allocate_subarray,
        generate_allocation_configuration,
    )
    from ska_ser_skallop.mvp_management.subarray_configuration import (
        configured_subarray,
        generate_scan_configuration,
    )
    from ska_ser_skallop.mvp_management.subarray_scanning import scanning_subarray
    from ska_ser_skallop.mvp_management.telescope_management import (
        running_telescope,
        TelState,
    )

    settings = types.ExecSettings()
    telescope_state = TelState()
    receptors = [1, 2]
    id = 1
    sb = types.SBConfig(sbid="temp")
    duration = 1
    alloc = generate_allocation_configuration("standard")
    scan_config = generate_scan_configuration("standard")

    with running_telescope(settings, telescope_state):
        with allocated_subarray(receptors, id, alloc, sb, settings):
            with configured_subarray(id, receptors, scan_config, sb, duration, settings):
                with scanning_subarray(id, receptors, settings):
                    alarm = generate_alarm_whilst_scanning()
                    verify_alarm_handled(alarm)

Note that :py:func:`generate_alarm_whilst_scanning` and :py:func:`verify_alarm_handled` are for demonstration 
purpose only and not real functions.

As shown in the code above there is an implied order in the way one context can be used within another:

#. :py:func:`running_telescope`
#. :py:func:`allocated_subarray`
#. :py:func:`configured_subarray`
#. :py:func:`scanning_subarray`

Should the user opt to directly call tear down for each setup, the equivalent code for above would look
as follows:

.. code-block:: python

    from ska_ser_skallop.mvp_management.telescope_management import (
        run_a_telescope,
        tear_down_a_telescope,
    )
    from ska_ser_skallop.mvp_management.subarray_composition import (
        compose_subarray,
        teardown_subarray,
    )
    from ska_ser_skallop.mvp_management.subarray_configuration import (
        configure_subarray,
        clear_subarray,
    )
    from ska_ser_skallop.mvp_management.subarray_scanning import (
        dispatch_scanning,
        clean_up_after_scan,
    )

    settings = types.ExecSettings()
    telescope_state = TelState()
    receptors = [1, 2]
    id = 1
    sb = types.SBConfig(sbid="temp")
    duration = 1
    alloc = generate_allocation_configuration("standard")
    scan_config = generate_scan_configuration("standard")

    run_a_telescope(settings)
    compose_subarray(receptors, id, alloc, sb, settings)
    configure_subarray(id, receptors, scan_config, sb, duration, settings)
    scanning_job = dispatch_scanning(id, receptors, settings)
    # end of setup code
    alarm = generate_alarm_whilst_scanning()
    verify_alarm_handled(alarm)
    # start of tear down code
    scanning_job.wait_for_scanning_to_complete()
    clean_up_after_scan(id, receptors, settings)
    clear_subarray(id, receptors, settings)
    teardown_subarray(receptors, id, settings)
    tear_down_a_telescope(settings)
