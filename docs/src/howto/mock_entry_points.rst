How to mock entry points 
========================
This section describes how to substitute the SUT with a mocked version should
in order to enable verifying the correctness of testing scripts before executing them on
the real device.

When to use it
--------------
When tooling (either from skallop) or from actual testing script are sophisticated or
unstable enough so as to warrant separate verification before executing the test on the
intended SUT.

How to use the feature
----------------------
The simplest way to enable this feature is to just set an env variable on the host:

.. code-block:: bash

    export MOCK_SUT=True

As a consequence of this setting, the fixtures will inject a
:py:class:`~ska_ser_skallop.mvp_control.entry_points.testing.MockedEntryPoint` object as the provider
for :py:class:`~ska_ser_skallop.mvp_control.entry_points.base.EntryPoint`. 
This entrypoint contains/references an 
:py:class:`~ska_ser_skallop.mvp_control.model.mvp_model.MVPModel` that gets used to emulate/mock SUT behaviour.

The :py:class:`~ska_ser_skallop.mvp_control.model.mvp_model.MVPModel`
is the object that must be exercised as a consequence of a particular command called on the entry point.
Behaviour is "mimicked" by means of a set of mock device proxies owned by the model for which a particular attribute value
can be set, resulting in consequent change events to subscribers and updated values for clients reading the state.
A user can set the state of the model on a high level per major subsystem (e.g tmc, sdp,csp) or on a particular tango device.

To override a particular entry point behaviour, the testing script can make use of the provided *"when\_"* decorators on the entry point.

The example below illustrate the concept.

.. code-block:: python

    from ska_ser_skallop.mvp_control.entry_points import testing


    @pytest.fixture(name="set_up_entry_point_if_mocked")
    def fxt_set_up_entry_point_if_mocked(mock_entry_point: testing.MockedEntryPoint):
        
        # only applies if env setting caused mocked entry point to be injected
        if mock_entry_point:
            @mock_entry_point.when_set_telescope_to_running
            def mock_set_telescope_to_running():
                mock_entry_point.model.set_states_for_telescope_running()

    @given("a configured entrypoint")
    def get_entry_point(set_up_entry_point_if_mocked):
        pass

    @when("I set the telescope to running")
    def set_to_running(entry_point):

        entry_point.set_telescope_to_running()

